# autolidar-teensyduino
Arduino libraries for Project Sprite.

Contains a `snapcraft.yaml` that will package:
- the Arduino IDE, patched with Teensyduino
- the Adafruit Unified Sensor Library (Adafruit_Sensor)
- the Adafruit Unified BNO055 Driver (Adafruit_BNO055)
